// example of an outside subscriber that only needs headRoll estimates from the publisher
// Author: RLV (UNITN)
// last update: 06/09/17

#include "zhelpers.hpp"
#include "zmq.hpp"
#include <iostream>

using namespace std;

int main () {
    //  Prepare our context and subscriber
    zmq::context_t context(1);
    zmq::socket_t subscriber (context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:6005");

    subscriber.setsockopt( ZMQ_SUBSCRIBE, "", 0);

    while (1) {

        //  Read envelope with address
        std::string address = s_recv (subscriber);
        //  Read message contents
        std::string contents = s_recv (subscriber);

        double localVal = std::stod(contents);

        cout << "[" << address << "] " << localVal << endl;

    }
    return 0;
}

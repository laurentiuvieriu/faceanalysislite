#ifndef SPEECHLOCALFUNCTIONS_H
#define SPEECHLOCALFUNCTIONS_H

#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <stdint.h>
#include "opencv2/core/core.hpp"
#include "LandmarkCoreIncludes.h"

namespace speech
{

# define M_PI           3.14159265358979323846  /* pi */

    using namespace std;
    using namespace cv;

    struct paramList
    {
        int windowSize;
        int featSize;
        int noTrees;
        Mat patchFeats;
    };

    struct randomTree
    {
        vector<double> cutVar, cutValue, rightChild, leafVal;
    };


    vector<string> get_all_files_names_within_folder(string folder);

    Mat RV_readCSVMat(string csvFrameName, int lin, int col);
    void writeMatToFile(cv::Mat& m, const char* filename);
    void RV_readParamList(const string foldername, paramList *p);
    vector<randomTree> RV_readForest(string forestDir, paramList params);
    vector<double> RV_computeHist(vector<double> vec, int noBins);

    double RV_testForest(Mat localData, vector<randomTree> &localForest, paramList params);

    double RV_computeMean(vector<double> vec);
//    void RV_featExtractionWholeFrame(Mat &frame, paramList params, Mat &result);
    void RV_featExtractionMouthCrop_lite(Mat &frame, paramList params, Mat &result);
    Mat RV_computeDiffFeats(vector<Mat> imageInt, vector<Mat> maskInt, Mat localFeat);
    Mat RV_computeRectangleArea(Mat imageInt, Mat localFeat);

    int RV_imshow(string windowName, Mat img);
    void shiftCol(Mat& out, Mat in, int numRight);
    Mat RV_computeSpeechFeats(Mat& featWindow);
    void RV_colwiseDiff(Mat& out, Mat in, int refCol);
    void RV_matrixStd(Mat& out, Mat in, int dim);

    void get_mouth_crop(cv::Mat& out, cv::Mat& in, const LandmarkDetector::CLNF& face_model);
}

#endif

// this program implements an autonomous camera_2 processor, that implements face-to-face interaction detection
// usage: <executable> <path_to_settings.ini>settings.ini
// Author: RLV (UNITN)
// last update: 28/08/17


// Local includes
#include "LandmarkCoreIncludes.h"

#include <Face_utils.h>
#include <FaceAnalyser.h>
#include <GazeEstimation.h>

#include "speechLocalFunctions.h"
#include "utilsSpeechCam2.h"
#include <boost/circular_buffer.hpp>

using namespace speech;


int main(int argc, char** argv) {

    string settings_file = "";

    if (argc != 2) {
        cout << "Usage: <path_to_exe>speechCam2 <settings_file>" << endl;
        return 1;
    } else {
        settings_file = argv[1];
    }

    vector<string> arguments = get_arguments_from_file(settings_file);

    // initialization procedure ...
    // Some initial parameters that can be overriden from command line
    vector<string> input_pubId, input_cam, input_files, depth_directories, output_files, tracked_videos_output;

    LandmarkDetector::FaceModelParameters det_parameters(arguments);
    // Always track gaze in feature extraction
    det_parameters.track_gaze = true;

    // Get the input output file parameters

    // Indicates that rotation should be with respect to camera or world coordinates
    bool use_world_coordinates;
    string output_codec; //not used but should
    LandmarkDetector::get_video_input_output_params(input_pubId, input_cam, input_files, depth_directories,
                                                    output_files, tracked_videos_output, use_world_coordinates,
                                                    output_codec, arguments);

    bool video_input = true;
    bool verbose = true;
    bool images_as_video = false;
    bool input_cam_flag = false;
    vector<vector<string> > input_image_files;

    if (input_files.empty()) {
        vector<string> d_files;
        vector<string> o_img;
        vector<cv::Rect_<double>> bboxes;
        get_image_input_output_params_feats(input_image_files, images_as_video, arguments);

        if (!input_image_files.empty()) {
            video_input = false;
        }

    }

    if (!input_cam.empty()) {
        input_cam_flag = true;
    }

    // Grab camera parameters, if they are not defined (approximate values will be used)
    float fx = 0, fy = 0, cx = 0, cy = 0;
    int d = 0;
    // Get camera parameters
    LandmarkDetector::get_camera_params(d, fx, fy, cx, cy, arguments);

    // If cx (optical axis centre) is undefined will use the image size/2 as an estimate
    bool cx_undefined = false;
    bool fx_undefined = false;
    if (cx == 0 || cy == 0) {
        cx_undefined = true;
    }
    if (fx == 0 || fy == 0) {
        fx_undefined = true;
    }

    // The modules that are being used for tracking
    LandmarkDetector::CLNF face_model(det_parameters.model_location);

    vector<string> output_similarity_align;
    vector<string> output_hog_align_files;

    double sim_scale = 0.7;
    int sim_size = 112;
    bool grayscale = false;
    bool video_output = false;
    bool dynamic = true; // Indicates if a dynamic AU model should be used (dynamic is useful if the video is long enough to include neutral expressions)
    int num_hog_rows;
    int num_hog_cols;

    // output parameters
    bool output_2D_landmarks = false;
    bool output_3D_landmarks = false;
    bool output_model_params = false;
    bool output_frame_idx = false;
    bool output_timestamp = false;
    bool output_confidence = false;
    bool output_success = false;
    bool output_head_position = false;
    bool output_head_pose = false;
    bool plot_head_pose = false;
    bool output_speech = false;
    bool output_f2f = false;

    get_output_feature_params(output_similarity_align, output_hog_align_files, sim_scale, sim_size, grayscale, verbose,
                              dynamic,
                              output_2D_landmarks, output_3D_landmarks, output_model_params, output_frame_idx,
                              output_timestamp,
                              output_confidence, output_success, output_head_position, output_head_pose, output_speech, output_f2f, arguments);

    // Used for image masking

    paramList params_speech;
    const string featDataRoot = get_key_from_arguments(arguments, "-rootDir_speech");
    RV_readParamList(featDataRoot, &params_speech);

    vector<randomTree> forest;
    const string treesDir = featDataRoot + "trees/";
    forest = RV_readForest(treesDir, params_speech);

    string tri_loc;
    if (boost::filesystem::exists(path("model/tris_68_full.txt"))) {
        tri_loc = "model/tris_68_full.txt";
    } else {
        path loc = path(arguments[0]).parent_path() / "model/tris_68_full.txt";
        tri_loc = loc.string();

        if (!exists(loc)) {
            cout << "Can't find triangulation files, exiting" << endl;
            return 1;
        }
    }

    // Will warp to scaled mean shape
    cv::Mat_<double> similarity_normalised_shape = face_model.pdm.mean_shape * sim_scale;
    // Discard the z component
    similarity_normalised_shape = similarity_normalised_shape(
            cv::Rect(0, 0, 1, 2 * similarity_normalised_shape.rows / 3)).clone();

    // If multiple video files are tracked, use this to indicate if we are done
    bool done = false;
    int f_n = -1;
    int curr_img = -1;

    string au_loc;

    string au_loc_local;
    if (dynamic) {
        au_loc_local = "AU_predictors/AU_all_best.txt";
    } else {
        au_loc_local = "AU_predictors/AU_all_static.txt";
    }

    if (boost::filesystem::exists(path(au_loc_local))) {
        au_loc = au_loc_local;
    } else {
        path loc = path(arguments[0]).parent_path() / au_loc_local;
        if (exists(loc)) {
            au_loc = loc.string();
        } else {
            cout << "Can't find AU prediction files, exiting" << endl;
            return 1;
        }
    }

    // Creating a  face analyser that will be used for AU extraction
    FaceAnalysis::FaceAnalyser face_analyser(vector<cv::Vec3d>(), 0.7, 112, 112, au_loc, tri_loc);

    boost::circular_buffer<double> speechVec(30);
    boost::circular_buffer<double> f2fVec(30);
    boost::circular_buffer<int> validWindowEntries(params_speech.windowSize);
    cv::Mat featData_speech;
    cv::Mat featWindow(params_speech.featSize, params_speech.windowSize, CV_64FC1);
    featWindow.setTo(0);
    double sm = 0;
    string::size_type sz_type;
    string buff = get_key_from_arguments(arguments, "-yaw_lim_left");
    double yawLim_left = std::stod(buff, &sz_type);
    buff = get_key_from_arguments(arguments, "-yaw_lim_right");
    double yawLim_right = std::stod(buff, &sz_type);
    buff = get_key_from_arguments(arguments, "-tilt_lim_up");
    double tiltLim_up = std::stod(buff, &sz_type);
    buff = get_key_from_arguments(arguments, "-tilt_lim_down");
    double tiltLim_down = std::stod(buff, &sz_type);
    buff = get_key_from_arguments(arguments, "-speechTh");
    double speechTh = std::stod(buff, &sz_type);

    // end of initialization ...

    VideoCapture cap;
    // open the default camera, use something different from 0 otherwise;
    // Check VideoCapture documentation.

    if (input_cam_flag){
        int camId = std::stoi(input_cam[0]);
        if (!cap.open(camId))
            return 0;
    }

    cv::Mat emo_neg  = cv::imread("../../../data/emo_neg_small.jpg"); // 110 x 120 pix
    cv::Mat emo_neu  = cv::imread("../../../data/emo_neu_small.jpg");
    cv::Mat emo_pos  = cv::imread("../../../data/emo_pos_small.jpg");

    while(!done)
    {
        string current_file;
        cv::VideoCapture video_capture;

        Mat captured_image;
        int total_frames = -1;
        int reported_completion = 0;

        double fps_vid_in = -1.0;

        if(video_input && !input_cam_flag){
            // We might specify multiple video files as arguments
            if(input_files.size() > 0)
            {
                f_n++;
                current_file = input_files[f_n];
            }
            else
            {
                // If we want to write out from webcam
                f_n = 0;
            }
            // Do some grabbing
            if( current_file.size() > 0 )
            {
                INFO_STREAM( "Attempting to read from file: " << current_file );
                video_capture = cv::VideoCapture( current_file );
                total_frames = (int)video_capture.get(CV_CAP_PROP_FRAME_COUNT);
                fps_vid_in = video_capture.get(CV_CAP_PROP_FPS);

                // Check if fps is nan or less than 0
                if (fps_vid_in != fps_vid_in || fps_vid_in <= 0)
                {
                    INFO_STREAM("FPS of the video file cannot be determined, assuming 30");
                    fps_vid_in = 30;
                }
            }

            if (!video_capture.isOpened())
            {
                FATAL_STREAM("Failed to open video source, exiting");
                return 1;
            }
            else
            {
                INFO_STREAM("Device or file opened");
            }

            video_capture >> captured_image;
        }
        if (!video_input && !input_cam_flag){
            f_n++;
            curr_img++;
            if(!input_image_files[f_n].empty())
            {
                string curr_img_file = input_image_files[f_n][curr_img];
                captured_image = cv::imread(curr_img_file, -1);
            }
            else
            {
                FATAL_STREAM( "No .jpg or .png images in a specified drectory, exiting" );
                return 1;
            }
        }

        if (input_cam_flag) {
            curr_img++;

            cap >> captured_image;
            if (captured_image.empty()) break;
        }

        // If optical centers are not defined just use center of image
        if(cx_undefined){
            cx = captured_image.cols / 2.0f;
            cy = captured_image.rows / 2.0f;
        }

        // Use a rough guess-timate of focal length
        if (fx_undefined){
            fx = 500 * (captured_image.cols / 640.0);
            fy = 500 * (captured_image.rows / 480.0);

            fx = (fx + fy) / 2.0;
            fy = fx;
        }

        // Creating output files
        std::ofstream output_file;

        if (!output_files.empty()){
            output_file.open(output_files[f_n], ios_base::out);
            prepareOutputFile(&output_file, output_2D_landmarks, output_3D_landmarks, output_model_params, output_frame_idx,
                              output_timestamp, output_confidence, output_success, output_head_position, output_head_pose, output_speech, output_f2f, face_model.pdm.NumberOfPoints(), face_model.pdm.NumberOfModes());
        }

        // Saving the HOG features
        std::ofstream hog_output_file;
        if(!output_hog_align_files.empty())
        {
            hog_output_file.open(output_hog_align_files[f_n], ios_base::out | ios_base::binary);
        }

        // saving the videos
        cv::VideoWriter writerFace;
        if(!tracked_videos_output.empty())
        {
            try
            {
                writerFace.open(tracked_videos_output[f_n], CV_FOURCC(output_codec[0],output_codec[1],output_codec[2],output_codec[3]), 30, captured_image.size(), true);
            }
            catch(cv::Exception e)
            {
                WARN_STREAM( "Could not open VideoWriter, OUTPUT FILE WILL NOT BE WRITTEN. Currently using codec " << output_codec << ", try using an other one (-oc option)");
            }


        }
        int frame_count = 0;

        // This is useful for a second pass run (if want AU predictions)
        vector<cv::Vec6d> params_global_video;
        vector<bool> successes_video;
        vector<cv::Mat_<double>> params_local_video;
        vector<cv::Mat_<double>> detected_landmarks_video;

        // Use for timestamping if using a webcam
        int64 t_initial = cv::getTickCount();

        bool visualise_hog = verbose;

        // Timestamp in seconds of current processing
        double time_stamp = 0;

        INFO_STREAM( "Starting tracking");
        while(!captured_image.empty())
        {

            // Grab the timestamp first
            if (video_input)
            {
                time_stamp = (double)frame_count * (1.0 / fps_vid_in);
            }
            else
            {
                // if loading images assume 30fps
                time_stamp = (double)frame_count * (1.0 / 30.0);
            }

            // Reading the images
            cv::Mat_<uchar> grayscale_image;

            if(captured_image.channels() == 3)
            {
                cvtColor(captured_image, grayscale_image, CV_BGR2GRAY);
            }
            else
            {
                grayscale_image = captured_image.clone();
            }

            // The actual facial landmark detection / tracking
            bool detection_success;

            if(video_input || images_as_video)
            {
                detection_success = LandmarkDetector::DetectLandmarksInVideo(grayscale_image, face_model, det_parameters);
            }
            else
            {
                detection_success = LandmarkDetector::DetectLandmarksInImage(grayscale_image, face_model, det_parameters);
            }

            // Gaze tracking, absolute gaze direction
            cv::Point3f gazeDirection0(0, 0, -1);
            cv::Point3f gazeDirection1(0, 0, -1);

            if (det_parameters.track_gaze && detection_success && face_model.eye_model)
            {
                FaceAnalysis::EstimateGaze(face_model, gazeDirection0, fx, fy, cx, cy, true);
                FaceAnalysis::EstimateGaze(face_model, gazeDirection1, fx, fy, cx, cy, false);
            }

            // Do face alignment
            cv::Mat sim_warped_img;
            cv::Mat_<double> hog_descriptor;

            // But only if needed in output
            if(!output_similarity_align.empty() || hog_output_file.is_open())
            {
                face_analyser.AddNextFrame(captured_image, face_model, time_stamp, false, !det_parameters.quiet_mode);
                face_analyser.GetLatestAlignedFace(sim_warped_img);

//                if(!det_parameters.quiet_mode)
//                {
//                    cv::imshow("sim_warp", sim_warped_img);
//                }
                if(hog_output_file.is_open())
                {
                    FaceAnalysis::Extract_FHOG_descriptor(hog_descriptor, sim_warped_img, num_hog_rows, num_hog_cols);

                    if(visualise_hog && !det_parameters.quiet_mode)
                    {
                        cv::Mat_<double> hog_descriptor_vis;
                        FaceAnalysis::Visualise_FHOG(hog_descriptor, num_hog_rows, num_hog_cols, hog_descriptor_vis);
                        cv::imshow("hog", hog_descriptor_vis);
                    }
                }
            }

            // Work out the pose of the head from the tracked model
            cv::Vec6d pose_estimate;
            if(use_world_coordinates)
            {
                pose_estimate = LandmarkDetector::GetCorrectedPoseWorld(face_model, fx, fy, cx, cy);
            }
            else
            {
                pose_estimate = LandmarkDetector::GetCorrectedPoseCamera(face_model, fx, fy, cx, cy);
            }

            if(hog_output_file.is_open())
            {
                output_HOG_frame(&hog_output_file, detection_success, hog_descriptor, num_hog_rows, num_hog_cols);
            }

            // Write the similarity normalised output
            if(!output_similarity_align.empty())
            {

                if (sim_warped_img.channels() == 3 && grayscale)
                {
                    cvtColor(sim_warped_img, sim_warped_img, CV_BGR2GRAY);
                }

                char name[100];

                // output the frame number
                std::sprintf(name, "frame_det_%06d.bmp", frame_count);

                // Construct the output filename
                boost::filesystem::path slash("/");

                std::string preferredSlash = slash.make_preferred().string();

                string out_file = output_similarity_align[f_n] + preferredSlash + string(name);
                bool write_success = imwrite(out_file, sim_warped_img);

                if (!write_success)
                {
                    cout << "Could not output similarity aligned image image" << endl;
                    return 1;
                }
            }

            // run here the pain detector

            double instant_speech = 0;
            int argMax_expression = 0;

            // speech part ...
            bool det_success = face_model.detection_success;
            cv::Mat mouthCrop(20, 40, CV_8UC3);
            mouthCrop.setTo(0);
            if (det_success) {
                validWindowEntries.push_back(1);
                get_mouth_crop(mouthCrop, captured_image, face_model);
                IplImage* rgb = new IplImage(mouthCrop);
                IplImage* gray = cvCreateImage(cvGetSize(rgb),IPL_DEPTH_8U,1);

                cvCvtColor(rgb, gray, COLOR_BGR2GRAY);

                Mat grayIn = cvarrToMat(gray);
                grayIn.convertTo(grayIn, CV_64FC1);

                RV_featExtractionMouthCrop_lite(grayIn, params_speech, featData_speech);
                shiftCol(featWindow, featWindow, -1);
                featData_speech.copyTo(featWindow(Rect(params_speech.windowSize- 1,0,1,featWindow.rows)));
            }
            else{
                validWindowEntries.push_back(0);
            }

            sm = std::accumulate(validWindowEntries.begin(), validWindowEntries.end(), 0.0);

            if (sm == params_speech.windowSize){
                cv::Mat feat = RV_computeSpeechFeats(featWindow);

                instant_speech = RV_testForest(feat, forest, params_speech);
            }

            int speechFlag = (instant_speech>=speechTh) ? 1:0;
            vector<double> headPoseVec(3);
            for (int kk = 3; kk < 6; kk++) {
                char poseC[255];
                double poseVal;
                if (kk >= 3) {
                    poseVal = (double) pose_estimate[kk] * 180 / CV_PI;
                } else {
                    poseVal = (double) pose_estimate[kk];
                }

                if (kk == 3) {
                    poseVal = (-1.0) * poseVal; // aligning tilt to the protocol
                }
                headPoseVec[kk- 3] = poseVal;
            }

            double localYaw = headPoseVec[1]; //(pose_estimate[4] * 180 / CV_PI);
            double localTilt = headPoseVec[0]; //((-1.0) * pose_estimate[3] * 180 / CV_PI);
            int headposeFlag = (localYaw <= yawLim_right) && (localYaw >= yawLim_left) && (localTilt <= tiltLim_up) && (localTilt >= tiltLim_down);
            f2fVec.push_back((speechFlag + headposeFlag)/2.0);
            speechVec.push_back(speechFlag);
            double speech_sum = std::accumulate(speechVec.begin(), speechVec.end(), 0.0);
            double speechLevel = speech_sum/speechVec.size();

            double f2fsum = std::accumulate(f2fVec.begin(), f2fVec.end(), 0.0);
            double f2fLevel = f2fsum/f2fVec.size();

            // Visualising the tracker
            visualise_tracking(captured_image, emo_neu, emo_neg, emo_pos, face_model, det_parameters, frame_count, fx, fy, cx, cy,
                               face_analyser, speechLevel, f2fLevel, plot_head_pose, mouthCrop);

            // Output the landmarks, pose, gaze, parameters and AUs
            if (output_file.is_open() && detection_success) {
                outputAllFeatures(&output_file, output_2D_landmarks, output_3D_landmarks, output_model_params,
                                  output_frame_idx,
                                  output_timestamp, output_confidence, output_success, output_head_position,
                                  output_head_pose, output_speech, output_f2f, face_model, frame_count, time_stamp,
                                  detection_success, pose_estimate, fx, fy, cx, cy, speechLevel, f2fLevel, face_analyser);
            }
            // output the tracked video
            if(!tracked_videos_output.empty()){
                writerFace << captured_image;
            }

            if(input_cam_flag){
                cap >> captured_image;
            }

            if(video_input && !input_cam_flag){
                video_capture >> captured_image;
            }
            if(!video_input && !input_cam_flag){
                curr_img++;
                if(curr_img < (int)input_image_files[f_n].size())
                {
                    string curr_img_file = input_image_files[f_n][curr_img];
                    captured_image = cv::imread(curr_img_file, -1);
                }
                else
                {
                    captured_image = cv::Mat();
                }
            }

            // detect key presses
            char character_press = cv::waitKey(1);

            // restart the tracker

            if(character_press == 'r')
            {
                face_model.Reset();
            }

            if (character_press=='d')
            {
                captured_image = cv::Mat();
                done = true;
            }
                // quit the application
            if(character_press=='q')
            {
                return(0);
            }

            // Update the frame count
            frame_count++;

        }

        if((video_input && f_n == input_files.size()- 1) || (!video_input && f_n == input_image_files.size() - 1))
        {
            done = true;
        }

        output_file.close();

//        if(output_files.size() > 0 && (output_AUs_reg || output_AUs_class))
//        {
//            cout << "Postprocessing the Action Unit predictions" << endl;
//            post_process_output_file(face_analyser, output_files[f_n], dynamic);
//        }
        // Reset the models for the next video
        face_analyser.Reset();
        face_model.Reset();

        frame_count = 0;
        curr_img = -1;
    }

    return 0;
}


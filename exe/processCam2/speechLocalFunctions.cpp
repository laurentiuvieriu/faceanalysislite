#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <stdint.h>
#include "opencv2/core/core.hpp"
#include "speechLocalFunctions.h"
#include <fstream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <map>
#include <boost/filesystem.hpp>
#include <math.h>
#include <iomanip>

const int draw_shiftbits = 4;
const int draw_multiplier = 1 << 4;

namespace speech
{

    using namespace std;
    using namespace cv;

    std::string to_string(int val)
    {
        std::stringstream str;
        str << val;
        return str.str();
    }

    vector<string> get_all_files_names_within_folder(string folder)
    {
        vector<string> names;

        namespace fs = boost::filesystem;
        fs::directory_iterator end_iter;
        if (fs::exists(folder) && fs::is_directory(folder))
        {
            for (fs::directory_iterator dir_iter(folder) ; dir_iter != end_iter ; ++dir_iter)
            {
                if (fs::is_regular_file(dir_iter->status()))
                {
                }
            }
        }

        return names;
    }


    Mat RV_readCSVMat(string csvFrameName, int lin, int col)
    {
        Mat output(lin, col, CV_64FC1);
        ifstream inFile(csvFrameName); //.c_str()); //csvFrameName
        bool existFlag = boost::filesystem::exists(csvFrameName);
        if(!existFlag)
        {
            cout << "Missing file: " << csvFrameName << endl;
            throw std::exception();
        }
        string line;
        int linenum = 0;
        while (getline(inFile, line))
        {
            linenum++;
            //cout << "\nLine #" << linenum << ":" << endl;
            istringstream linestream(line);
            string item;
            int itemnum = 0;
            while (getline(linestream, item, ','))
            {
                itemnum++;
                //cout << "Item #" << itemnum << ": " << item << endl;
                double temp = (double)atof(item.c_str());
                output.at<double>(linenum - 1, itemnum - 1) = (double)temp;
            }
        }
        return output;
    }

    void writeMatToFile(cv::Mat& m, const char* filename)
    {
        ofstream fout(filename);

        if(!fout)
        {
            cout<<"File Not Opened"<<endl;  return;
        }

        for(int i=0; i<m.rows; i++)
        {
            for(int j=0; j<m.cols; j++)
            {
                fout << std::fixed << std::setprecision(12) << m.at<double>(i,j) << ",";
            }
            fout<<endl;
        }

        fout.close();
    }

    void RV_readParamList(const string foldername, paramList *p)
    {
        cout << "Reading params from: " << foldername << "...";
        ifstream in(foldername + "config.txt");
        string dummy;

        if (in.is_open()) {

            in >> dummy;
            in >> p->windowSize;

            in >> dummy;
            in >> p->featSize;

            in >> dummy;
            in >> p->noTrees;

        }
        else {
            cerr << "File not found " << foldername + "config.txt" << endl;
            exit(-1);
        }
        in.close();

//        Mat dummyMat(p->featSize, 8, CV_64FC1);
//        dummyMat.setTo(0);
//        p->patchFeats = dummyMat;

        Mat paramsPatchCoords = RV_readCSVMat(foldername + "paramsPatchCoords.csv", 1, 2);
        //p->patchCoords = paramsPatchCoords;

        // rectangle coordinates are imported from Matlab as they are, which means they obey Matlab indexing (starting from 1)
        // this will be taken care of explicitly at feature computation stage ...
        Mat patchFeatsAux = RV_readCSVMat(foldername + "patchFeats/patchFeats_1.csv", p->featSize, 8);
        Mat patchFeatsAuxInt(patchFeatsAux.rows, patchFeatsAux.cols, CV_8UC1);
        patchFeatsAux.convertTo(patchFeatsAuxInt, CV_8UC1);
        p->patchFeats = patchFeatsAuxInt;

        cout << "done!" << endl;
        cout << endl << "------------------------------------" << endl;
//        cout << "Parameter List:" << endl << endl;
//        cout << "d:             " << p->d << endl;
//        cout << "patchSize:     " << p->patchSize << endl;
//        cout << "stride:        " << p->stride << endl;
//        cout << "featSize:      " << p->featSize << endl;
//        cout << "noPatches:     " << p->noPatches << endl;
//        cout << "noChannels:    " << p->noChannels << endl;
//        cout << endl << "------------------------------------" << endl << endl;
    }

    vector<randomTree> RV_readForest(string forestDir, paramList params)
    {
        vector<randomTree> forest(params.noTrees);
        cout << "Reading forest from: " << forestDir << "...";
        for (int j = 0; j < params.noTrees; j++)
        {
            Mat dummySize = RV_readCSVMat(forestDir + "tree_" + to_string(j + 1) + "_size.csv", 1, 1);
            Mat dummyMat = RV_readCSVMat(forestDir + "tree_" + to_string(j + 1) + ".csv", 4, (int)dummySize.at<double>(0, 0));
            forest[j].cutVar.reserve(dummyMat.cols);
            forest[j].cutValue.reserve(dummyMat.cols);
            forest[j].rightChild.reserve(dummyMat.cols);
            forest[j].leafVal.reserve(dummyMat.cols);
            dummyMat.row(0).copyTo(forest[j].cutVar);
            dummyMat.row(1).copyTo(forest[j].cutValue);
            dummyMat.row(2).copyTo(forest[j].rightChild);
            dummyMat.row(3).copyTo(forest[j].leafVal);
        }
        cout << "done!" << endl;
        return forest;
    }

    vector<double> RV_computeHist(vector<double> vec, int noBins)
    {
        // supposedly vec has exactly noBins unique values
        const double eps = 1.0 / vec.size();
        vector<double> out(noBins);
        out.assign(noBins, 0);
        for (int i = 0; i < (int)vec.size(); i++)
        {
            out[(int)vec[i] - 1] += eps;
        }
        return out;
    }

    double RV_testForest(Mat localData, vector<randomTree> &localForest, paramList params)
    {
        vector<double> votes(params.noTrees);
        votes.assign(params.noTrees, 0);
        for (int k = 0; k < params.noTrees; k++)
        {
            randomTree tree = localForest[k];
            int currNode = 0;

            double val = tree.cutVar[currNode];

            while (tree.cutVar[currNode] > 0)
            {
                if (localData.at<double>(tree.cutVar[currNode]- 1) > tree.cutValue[currNode])
                {
                    currNode = tree.rightChild[currNode];
                }
                else
                {
                    currNode = tree.rightChild[currNode] - 1;
                }
            }
            votes[k] = tree.leafVal[currNode];
        }
        double probs = RV_computeMean(votes);
        return probs;
    }

    double RV_computeMean(vector<double> vec)
    {
        double sum = 0;
        for (int k = 0; k < (int)vec.size(); k++)
        {
            sum += vec[k];
        }
        return sum / vec.size();
    }

    void RV_featExtractionMouthCrop_lite(Mat &frame, paramList params, Mat &featData)
    {

        Mat dummyMat(1, (int)(params.featSize), CV_64FC1);
        dummyMat.setTo(0);

        int rows = frame.rows;
        int cols = frame.cols;

        Mat localChannel = frame;
        Mat channelInt(rows + 1, cols + 1, CV_64FC1);
        channelInt.setTo(0);
        integral(localChannel, channelInt);

        localChannel.convertTo(localChannel, CV_32FC1);

        Mat localImageInt(channelInt, Rect(1, 1, cols, rows));

        Mat feats = RV_computeRectangleArea(localImageInt, params.patchFeats);
        feats.copyTo(featData);
        // return featData;
    }

    Mat RV_computeRectangleArea(Mat imageInt, Mat localFeat)
    {
        Mat feats(localFeat.rows, 1, CV_64FC1);
        feats.setTo(0);


        Mat localImg = imageInt;
        for (int k = 0; k < localFeat.rows; k++)
        {
            // keep in mind that the coordinates of the rectangles are obeying Matlab indexing, therefore adaptation is performed explicitly here

            double a1 = localImg.at<double>(localFeat.at<uchar>(k, 3) - 1, localFeat.at<uchar>(k, 2) - 1) -
                        localImg.at<double>(localFeat.at<uchar>(k, 3) - 1, localFeat.at<uchar>(k, 0) - 2) -
                        localImg.at<double>(localFeat.at<uchar>(k, 1) - 2, localFeat.at<uchar>(k, 2) - 1) +
                        localImg.at<double>(localFeat.at<uchar>(k, 1) - 2, localFeat.at<uchar>(k, 0) - 2);
            double featVal = a1;
            feats.at<double>(k) = featVal;
        }

        return feats;
    }

    int RV_imshow(string windowName, Mat img)
    {
        double minVal, maxVal;
        Mat draw(img.rows, img.cols, CV_8UC1);
        minMaxLoc(img, &minVal, &maxVal);
        img.convertTo(draw, CV_8UC1, 255.0 / (maxVal - minVal), -minVal * 255.0 / (maxVal - minVal));
        imshow(windowName, draw);
        return 1;
    }

    void shiftCol(Mat& out, Mat in, int numRight){
        if(numRight == 0){
            in.copyTo(out);
            return;
        }

        int ncols = in.cols;
        int nrows = in.rows;

        Mat local_out = Mat::zeros(in.size(), in.type());

        numRight = numRight%ncols;
        if(numRight < 0)
            numRight = ncols+numRight;

        in(cv::Rect(ncols-numRight,0, numRight,nrows)).copyTo(local_out(cv::Rect(0,0,numRight,nrows)));
        in(cv::Rect(0,0, ncols-numRight,nrows)).copyTo(local_out(cv::Rect(numRight,0,ncols-numRight,nrows)));

        local_out.copyTo(out);
    }

    Mat RV_computeSpeechFeats(Mat& featWindow){
        int cols = featWindow.cols;
        int rows = featWindow.rows;
        Mat feat(rows*3, 1, CV_64FC1);
        feat.setTo(0);

        Mat diffMat(rows, cols- 1, CV_64FC1);
        diffMat.setTo(0);
        RV_colwiseDiff(diffMat, featWindow, 0);

        Mat diffMean;
        reduce(diffMat, diffMean, 1, CV_REDUCE_AVG);
        Mat diffStd(rows, 1, CV_64FC1);
        RV_matrixStd(diffStd, diffMat, 1);
        Mat diffLocal_1(diffMat(CvRect(1, 0, cols- 2, rows)));
        Mat diffLocal_2(diffMat(CvRect(0, 0, cols- 2, rows)));
        Mat diffMat2(diffLocal_1- diffLocal_2);
        Mat diff2Mean;
        reduce(abs(diffMat2), diff2Mean, 1, CV_REDUCE_AVG);
        diffMean.copyTo(feat(CvRect(0, 0, 1, rows)));
        diffStd.copyTo(feat(CvRect(0, rows, 1, rows)));
        diff2Mean.copyTo(feat(CvRect(0, 2* rows, 1, rows)));

//        writeMatToFile(feat, "/home/radu/work/cpp/201707_acantoFaceAnalysis/temp/faceanalysislite/exe/speechCam2/data/featData.txt");

        return feat;
    }

    void RV_colwiseDiff(Mat& out, Mat in, int refCol){
        int nrows = in.rows;

        int localCol = 0;
        for (int i= 0; i< in.cols; i++){
            if (i != refCol){
                Mat diffMat = in(CvRect(i, 0, 1, nrows)) - in(CvRect(refCol, 0, 1, nrows));
                diffMat.copyTo(out(CvRect(localCol, 0, 1, nrows)));
                localCol++;
            }
        }
    }

    void RV_matrixStd(Mat& out, Mat in, int dim){
        if (dim == 0){
            // compute std over the rows (result will be a vector of stds of size no_cols)
            for (int i= 0; i< in.cols; i++){
                Scalar dummy, stddev;
                cv::meanStdDev(in(cvRect(i, 0, 1, in.rows)), dummy, stddev);
                out.at<double>(i) = sqrt(pow(stddev.val[0], 2)*in.cols/(in.cols- 1));
            }
        }
        else{
            // compute std over the cols (result will be a vector of stds of size no_rows)
            for (int i= 0; i< in.rows; i++){
                Scalar dummy, stddev;
                cv::meanStdDev(in(cvRect(0, i, in.cols, 1)), dummy, stddev);
                out.at<double>(i) = sqrt(pow(stddev.val[0], 2)*in.cols/(in.cols- 1));
            }
        }
    }

    void get_mouth_crop(cv::Mat& out, cv::Mat& in, const LandmarkDetector::CLNF& face_model){
        vector<int> reducedPointSet = {49,51,53,55,58}; // left corner, up left, up right, right corner, down middle
        bool reducedSetFlag = true;

        const cv::Mat_<double> shape2D = face_model.detected_landmarks;
        int n = shape2D.rows/2;

        vector<cv::Point> mouthPoints;

        if (reducedSetFlag){
            for( int k = 0; k < reducedPointSet.size(); ++k) {
                int i = reducedPointSet[k] - 1;

                cv::Point featurePoint(cvRound(shape2D.at<double>(i) * (double) draw_multiplier/ 16),
                                       cvRound(shape2D.at<double>(i + n) * (double) draw_multiplier/ 16));

                mouthPoints.push_back(featurePoint);
            }
        }

        double dist_w = std::max(0, mouthPoints[3].x - mouthPoints[0].x);
        double dist_h = std::max(0, mouthPoints[4].y - (mouthPoints[1].y + mouthPoints[2].y)/2);
        double offset_w = dist_w/6;
        double offset_h = dist_h/6;

        int leftCorner_x = std::max(0, cvRound(mouthPoints[0].x - offset_w));
        int leftCorner_y = std::max(0, cvRound((mouthPoints[1].y + mouthPoints[2].y)/2 - offset_h));
        int rightCorner_x = std::min(in.cols- 1, cvRound(mouthPoints[3].x + offset_w));
        int rightCorner_y = std::min(in.rows- 1, cvRound(mouthPoints[4].y + offset_h));

        if ((leftCorner_x > rightCorner_x) || (leftCorner_y > rightCorner_y)){
            out.setTo(0);
        }
        else{
            cv::Mat crop(in(cvRect(leftCorner_x, leftCorner_y, rightCorner_x- leftCorner_x+ 1, rightCorner_y- leftCorner_y+ 1)));
            cv::Size sz(out.cols, out.rows);
            cv::resize(crop, out, sz);
        }
    }
}
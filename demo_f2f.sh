#!/bin/bash

echo "Firing the f2f subscriber in a new terminal ..."

gnome-terminal -e ./build/exe/faceSub/faceSub&

echo "done! Now launching the f2f publisher (main component) in a second terminal ..."

gnome-terminal -x /bin/bash -c "cd ./build/exe/processSub2/; ./processSub2 -cam 0 -settings ../../../exe/processSub2/settings.ini -subPort tcp://*:6005 -debugMode 1"

echo "done"&
